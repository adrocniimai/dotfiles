" stop acting like classic vi
set nocompatible
set history=100
set noswapfile
set nobackup

" enable filetype
filetype indent plugin on

" persist undo history between file editing sessions.
set undofile
set undodir=~/.config/nvim/undo

" modify indenting settings
set autoindent

" modify some other settings about files
set encoding=utf-8
set backspace=indent,eol,start
set hidden

" colorscheme configuration
if &t_Co > 2
	syntax on
	set background=dark

	highlight Folded cterm=reverse ctermbg=0 ctermfg=8
	highlight VertSplit cterm=NONE ctermbg=0 ctermfg=8
	highlight Conceal cterm=NONE ctermbg=0 ctermfg=8

	highlight DiffAdd ctermfg=green cterm=bold
	highlight DiffDelete ctermfg=red cterm=bold
	highlight DiffChange ctermfg=yellow
	highlight ColorColumn ctermbg=0 guibg=grey

	" sometimes I see the syntax be out of sync
	noremap <F12> <ESC>:syntax sync fromstart<CR>
	inoremap <C-o> <ESC>:syntax sync fromstart<CR>
endif

" mark trailing spaces depending on whether we have a fancy terminal or not
if &t_Co > 2
	highlight ExtraWhitespace ctermbg=1
	match ExtraWhitespace /\s\+$/
else
	set listchars=trail:~
	set list
endif

" use a specific pipe ch
set fillchars+=vert:\┊

" other config
set noshowmode
set laststatus=1
set wildmenu

set nowrap
set number
set relativenumber
set showmatch

let mapleader=","

" clean hidden buffers
function DeleteHiddenBuffers()
    let tpbl=[]
    call map(range(1, tabpagenr('$')), 'extend(tpbl, tabpagebuflist(v:val))')
    for buf in filter(range(1, bufnr('$')), 'bufexists(v:val) && index(tpbl, v:val)==-1')
        silent execute 'bwipeout' buf
    endfor
endfunction


inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O
