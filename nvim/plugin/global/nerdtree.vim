autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

map <C-\> :NERDTreeToggle<CR>
map <C-Right> :tabn<CR>
map <C-Left> :tabp<CR>

let g:NERDTreeMapCustomOpen = '<CR>'
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
